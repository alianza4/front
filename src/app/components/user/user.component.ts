import { DialogDeleteComponent } from '../dialog-delete/dialog-delete.component';
import { UserService } from './../../services/user.service';
import { User } from './../../models/user';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestResult } from 'src/app/models/requestResult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  hide = true;
  user: User = new User();
  id: string = '';

  registerForm = this.fb.group({
    id: [''],
    name: ['', Validators.required],
    lastName: ['', Validators.required],
    phone: [''],
    email: [''],
    password: ['', Validators.required],
    idRol: ['3c6639d8-5bb4-4df8-b65f-24a221a04058'],
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router
  ) {
    this.id = this.route.snapshot.paramMap.get('id') as string;
  }

  ngOnInit(): void {
    this.userService.get(this.id).subscribe((rq) => {
      if (rq.successful) {
        this.user = rq.result;
        this.registerForm.patchValue({
          id: this.user.id,
          name: this.user.name,
          lastName: this.user.lastName,
          phone: this.user.phone,
          email: this.user.email,
          password: this.user.password,
          idRol: this.user.idRol,
        });
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  update() {
    if (this.registerForm.invalid) return;

    this.userService.update(this.registerForm.value).subscribe((rq) => {
      if (rq.successful) {
        this.snackBar.open('Actualizado correctamente', 'Cerrar', {
          duration: 3000,
          panelClass: ['snackbar-success'],
        });
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  deleteConfirm(): void {
    this.dialog
      .open(DialogDeleteComponent, {
        data: `¿Estas seguro de eliminar el usuario?`,
      })
      .afterClosed()
      .subscribe((confirmado: Boolean) => {
        if (confirmado) {
          this.delete();
        }
      });
  }

  delete() {
    this.userService.delete(this.user.id).subscribe((rq) => {
      if (rq.successful) {
        this.snackBar.open('Se ha elimnado el usuario', 'Cerrar', {
          duration: 3000,
          panelClass: ['snackbar-success'],
        });

        localStorage.clear();
        this.router.navigateByUrl('home');
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
