import { Description } from '../models/description';
import { Property } from './../models/property';
import { RequestResult } from './../models/requestResult';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PropertyService {
  url = 'http://localhost:8089/alianza/api';

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http
      .get<RequestResult<Property[]>>(`${this.url}/property/getAll`)
      .pipe(take(1));
  }

  get(id: string) {
    let params = new HttpParams().set('id', id);

    return this.http
      .get<RequestResult<Property>>(`${this.url}/property/get`, { params })
      .pipe(take(1));
  }

  getByUser(id: string) {
    let params = new HttpParams().set('id', id);
    return this.http
      .get<RequestResult<Property[]>>(`${this.url}/property/getByUser`, {
        params,
      })
      .pipe(take(1));
  }

  delete(id: string) {
    let params = new HttpParams().set('id', id);
    return this.http
      .delete<RequestResult<boolean>>(`${this.url}/property/delete`, { params })
      .pipe(take(1));
  }

  save(property: Property) {
    return this.http
      .post<RequestResult<Property>>(`${this.url}/property/save`, property)
      .pipe(take(1));
  }

  getTypeProperties() {
    return this.http
      .get<RequestResult<Description[]>>(`${this.url}/typeProperty/getAll`)
      .pipe(take(1));
  }

  getCities() {
    return this.http
      .get<RequestResult<Description[]>>(`${this.url}/city/getAll`)
      .pipe(take(1));
  }

  getByCity(idCity: string) {
    var params = new HttpParams().set('idCity', idCity);

    return this.http
      .get<RequestResult<Description[]>>(
        `${this.url}/neighbourhood/getByCity`,
        { params }
      )
      .pipe(take(1));
  }
}
