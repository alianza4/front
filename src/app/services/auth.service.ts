import { UserToken } from './../models/userToken';
import { RequestResult } from './../models/requestResult';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  url: string = 'http://localhost:8089/alianza/api';

  constructor(private http: HttpClient) {}

  login(loginRequest: { email: string; password: string }) {
    return this.http
      .post<RequestResult<UserToken>>(`${this.url}/user/login`, loginRequest)
      .pipe(take(1));
  }

  register(user: User) {
    return this.http
      .post<RequestResult<UserToken>>(`${this.url}/user/save`, user)
      .pipe(take(1));
  }
}
