import { CommissionService } from './../../services/commission.service';
import { UserToken } from './../../models/userToken';
import { Property } from 'src/app/models/property';
import { PropertyService } from './../../services/property.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RequestResult } from 'src/app/models/requestResult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder } from '@angular/forms';
import { SESSION } from 'src/app/models/const';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-property-detail',
  templateUrl: './property-detail.component.html',
  styleUrls: ['./property-detail.component.scss'],
})
export class PropertyDetailComponent implements OnInit {
  id!: string;
  property!: Property;
  comercial!: User;

  saleForm = this.fb.group({
    id: [''],
    amount: [0],
    comment: [''],
    name: [''],
    lastName: [''],
    buyer: [''],
    idProperty: [''],
    idUser: [''],
  });

  constructor(
    private route: ActivatedRoute,
    private propertyService: PropertyService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private commissionService: CommissionService,
    private userService: UserService
  ) {
    this.id = this.route.snapshot.paramMap.get('id')!;
  }

  ngOnInit(): void {
    let user: UserToken = JSON.parse(atob(localStorage.getItem(SESSION)!));

    this.saleForm.patchValue({
      name: user.name,
      lastName: user.lastName,
      idProperty: this.id,
    });

    this.getProp();
  }

  getComercial(ids: string[]) {
    this.userService.getComercial(ids).subscribe((rq) => {
      if (rq.successful) {
        this.comercial = rq.result;
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  sale() {
    if (this.saleForm.invalid) return;

    const amount = (this.property.price / 100) * 10;
    this.saleForm.patchValue({
      amount: amount,
      buyer: `${this.saleForm.value.name} ${this.saleForm.value.lastName}`,
    });

    this.commissionService.sale(this.saleForm.value).subscribe((rq) => {
      if (rq.successful) {
        this.snackBar.open('Felicitaciones', 'Cerrar', {
          duration: 3000,
          panelClass: ['snackbar-success'],
        });

        this.getProp();
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  getProp() {
    this.propertyService.get(this.id).subscribe((rq) => {
      if (rq.successful) {
        this.property = rq.result;
        let ids: string[] = [];
        this.property.userProperties.forEach((user) => ids.push(user.idUser));
        this.getComercial(ids);
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
