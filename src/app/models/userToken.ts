import { Rol } from './rol';
export class UserToken {
  id: string;
  name: string;
  lastName: string;
  phone: string;
  email: string;
  password: string;
  idRol: string;
  rol: Rol;
  token: string;
  expiration: Date;

  constructor() {
    this.id = '';
    this.name = '';
    this.lastName = '';
    this.phone = '';
    this.email = '';
    this.password = '';
    this.idRol = '';
    this.rol = new Rol();
    this.token = '';
    this.expiration = new Date();
  }
}
