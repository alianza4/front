import { OfferType, StateProperty } from './enum';
export class Property {
  id: string;
  offerType: OfferType;
  idTypeProperty: string;
  idCity: string;
  idNeighbourhood: string;
  address: string;
  price: number;
  stratum: number;
  area: number;
  administrationInPrice: boolean;
  rooms: number;
  bathrooms: number;
  floors: number;
  description: string;
  idImage: string;
  parking: boolean;
  idUser: string;
  city: City;
  neighbourhood: Neighbourhood;
  image: null;
  typeProperty: TypeProperty;
  userProperties: UserProperty[];
  state: StateProperty;

  constructor() {
    this.id = '';
    this.idTypeProperty = '';
    this.idCity = '';
    this.idNeighbourhood = '';
    this.address = '';
    this.price = 0;
    this.stratum = 0;
    this.area = 0;
    this.administrationInPrice = false;
    this.rooms = 0;
    this.bathrooms = 0;
    this.floors = 0;
    this.description = '';
    this.idImage = '';
    this.parking = false;
    this.idUser = '';
    this.city = new City();
    this.neighbourhood = new Neighbourhood();
    this.image = null;
    this.typeProperty = new TypeProperty();
    this.userProperties = [];
    this.offerType = OfferType.SALE;
    this.state = StateProperty.SALE;
  }
}

class City {
  id: string;
  description: string;

  constructor() {
    this.id = '';
    this.description = '';
  }
}

class Neighbourhood {
  id: string;
  description: string;
  idCity: string;

  constructor() {
    this.id = '';
    this.description = '';
    this.idCity = '';
  }
}

class TypeProperty {
  id: string;
  description: string;

  constructor() {
    this.id = '';
    this.description = '';
  }
}

class UserProperty {
  idUser: string;
  idProperty: string;

  constructor() {
    this.idUser = '';
    this.idProperty = '';
  }
}
