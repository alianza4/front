import { AddUserComponent } from './add-user/add-user.component';
import { UserService } from './../../services/user.service';
import { User } from './../../models/user';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { RequestResult } from 'src/app/models/requestResult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { DialogDeleteComponent } from '../dialog-delete/dialog-delete.component';

@Component({
  selector: 'app-users-admin',
  templateUrl: './users-admin.component.html',
  styleUrls: ['./users-admin.component.scss'],
})
export class UsersAdminComponent implements OnInit {
  dataSource!: MatTableDataSource<User>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  users: User[] = [];

  displayedColumns: string[] = [
    'name',
    'lastName',
    'phone',
    'email',
    'rol',
    'actions',
  ];

  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.userService.getAll().subscribe((rq) => {
      if (rq.successful) {
        this.users = rq.result;
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editProp(row: any) {
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '600px',
      data: { name: row },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getUsers();
    });
  }

  deleteProp(row: any) {
    this.dialog
      .open(DialogDeleteComponent, {
        data: `¿Estas seguro de eliminar al usuario?`,
      })
      .afterClosed()
      .subscribe((confirmado: Boolean) => {
        if (confirmado) {
          this.userService.delete(row.id).subscribe((rq) => {
            if (rq.successful) {
              this.getUsers();

              this.snackBar.open('Eliminado correctamente', 'Cerrar', {
                duration: 3000,
                panelClass: ['snackbar-success'],
              });
            } else {
              this.validateRequestResult(rq);
            }
          });
        }
      });
  }

  openDialog() {
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '600px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getUsers();
    });
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
