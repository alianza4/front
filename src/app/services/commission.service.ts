import { Commission } from './../models/commission';
import { RequestResult } from './../models/requestResult';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CommissionService {
  url = 'http://localhost:8089/alianza/api';

  constructor(private http: HttpClient) {}

  sale(commission: Commission) {
    return this.http
      .post<RequestResult<Commission>>(
        `${this.url}/commission/sale`,
        commission
      )
      .pipe(take(1));
  }

  getCommissions(idUser: string) {
    let params = new HttpParams().set('idUser', idUser);
    return this.http
      .get<RequestResult<Commission[]>>(
        `${this.url}/commission/getCommissions`,
        { params }
      )
      .pipe(take(1));
  }

  exportReport(id: string) {
    let params = new HttpParams().set('id', id);
    return this.http
      .get(`${this.url}/commission/export`, { params, responseType: 'blob' })
      .pipe(take(1));
  }
}
