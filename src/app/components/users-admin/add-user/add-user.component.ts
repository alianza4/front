import { Rol } from './../../../models/rol';
import { UserService } from './../../../services/user.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RequestResult } from 'src/app/models/requestResult';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  roles: Rol[] = [];
  hide = true;
  registerForm = this.fb.group({
    id: [''],
    name: [''],
    lastName: [''],
    phone: [''],
    email: [''],
    password: [''],
    idRol: [''],
  });

  constructor(
    public fb: FormBuilder,
    public dialogRef: MatDialogRef<AddUserComponent>,
    private userService: UserService,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.getAllRoles();
    if (this.data) {
      this.registerForm.patchValue(this.data.name);
    }
  }

  register() {
    if (this.registerForm.invalid) return;

    this.userService.update(this.registerForm.value).subscribe((rq) => {
      if (rq.successful) {
        this.snackBar.open('Creado correctamente', 'Cerrar', {
          duration: 3000,
          panelClass: ['snackbar-success'],
        });

        this.dialogRef.close();
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  getAllRoles() {
    this.userService.getAllRoles().subscribe((rq) => {
      if (rq.successful) {
        this.roles = rq.result;
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
