import { Rol } from './rol';
export class User {
  id: string;
  name: string;
  lastName: string;
  phone: string;
  email: string;
  password: string;
  idRol: string;
  rol: Rol;

  constructor() {
    this.id = '';
    this.name = '';
    this.lastName = '';
    this.phone = '';
    this.email = '';
    this.password = '';
    this.idRol = '';
    this.rol = new Rol();
  }
}
