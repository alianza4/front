import { PropertyService } from './../../services/property.service';
import { Property } from 'src/app/models/property';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { RequestResult } from 'src/app/models/requestResult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { AddPropComponent } from './add-prop/add-prop.component';
import { DialogDeleteComponent } from '../dialog-delete/dialog-delete.component';

@Component({
  selector: 'app-user-properties',
  templateUrl: './user-properties.component.html',
  styleUrls: ['./user-properties.component.scss'],
})
export class UserPropertiesComponent implements OnInit {
  displayedColumns: string[] = [
    'offerType',
    'typeProperty',
    'city',
    'neighbourhood',
    'address',
    'price',
    'stratum',
    'area',
    'description',
    'actions',
  ];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  dataSource!: MatTableDataSource<Property>;
  listProperties: Property[] = [];

  constructor(
    private propertyService: PropertyService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getProps();
  }

  getProps() {
    this.propertyService
      .getByUser(this.route.snapshot.paramMap.get('id') as string)
      .subscribe((rq) => {
        if (rq.successful) {
          this.listProperties = rq.result;
          this.dataSource = new MatTableDataSource(this.listProperties);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        } else {
          this.validateRequestResult(rq);
        }
      });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddPropComponent, {
      width: '600px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getProps();
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  deleteProp(row: any) {
    this.dialog
      .open(DialogDeleteComponent, {
        data: `¿Estas seguro de eliminar la propiedad?`,
      })
      .afterClosed()
      .subscribe((confirmado: Boolean) => {
        if (confirmado) {
          this.propertyService.delete(row.id).subscribe((rq) => {
            if (rq.successful) {
              this.getProps();

              this.snackBar.open('Eliminado correctamente', 'Cerrar', {
                duration: 3000,
                panelClass: ['snackbar-success'],
              });
            } else {
              this.validateRequestResult(rq);
            }
          });
        }
      });
  }

  editProp(row: any) {
    const dialogRef = this.dialog.open(AddPropComponent, {
      width: '600px',
      data: { name: row },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getProps();
    });
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
