import { Rol } from './../models/rol';
import { RequestResult } from './../models/requestResult';
import { User } from './../models/user';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  url: string = 'http://localhost:8089/alianza/api';

  constructor(private http: HttpClient) {}

  update(user: User) {
    return this.http
      .post<RequestResult<User>>(`${this.url}/user/update`, user)
      .pipe(take(1));
  }

  get(id: string) {
    let params = new HttpParams().set('id', id);
    return this.http
      .get<RequestResult<User>>(`${this.url}/user/get`, { params })
      .pipe(take(1));
  }

  delete(id: string) {
    let params = new HttpParams().set('id', id);
    return this.http
      .delete<RequestResult<boolean>>(`${this.url}/user/delete`, { params })
      .pipe(take(1));
  }

  getAll() {
    return this.http
      .get<RequestResult<User[]>>(`${this.url}/user/getAll`)
      .pipe(take(1));
  }

  getAllRoles() {
    return this.http
      .get<RequestResult<Rol[]>>(`${this.url}/user/getAllRoles`)
      .pipe(take(1));
  }

  getComercial(ids: string[]) {
    return this.http
      .post<RequestResult<User>>(`${this.url}/user/getComercial`, ids)
      .pipe(take(1));
  }
}
