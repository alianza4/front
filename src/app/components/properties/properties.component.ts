import { PropertyService } from './../../services/property.service';
import { Component, OnInit } from '@angular/core';
import { Property } from 'src/app/models/property';
import { RequestResult } from 'src/app/models/requestResult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { StateProperty } from 'src/app/models/enum';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss'],
})
export class PropertiesComponent implements OnInit {
  filter: FormControl = new FormControl('');
  properties: Property[] = [];

  constructor(
    private propertyService: PropertyService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.propertyService.getAll().subscribe((rq) => {
      if (rq.successful) {
        this.properties = rq.result.filter(
          (p) => p.state === StateProperty.OFFER
        );
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
