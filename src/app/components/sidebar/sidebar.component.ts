import { UserToken } from 'src/app/models/userToken';
import { Action } from './../../models/action';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducers';
import { SESSION } from 'src/app/models/const';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  @ViewChild('sidenav') sidenav!: MatSidenav;
  listActions!: Action[];

  constructor(private store: Store<AppState>, private router: Router) {
    let LS = localStorage.getItem(SESSION)!;
    if (LS) {
      var user64 = atob(LS);
      var user: UserToken = JSON.parse(user64);

      this.listActions = [
        {
          icon: 'maps_home_work',
          name: 'Mis propiedades',
          visible: true,
          message: 'Configure sus propiedades',
          redirect: `user-properties/${user.id}`,
        },
        {
          icon: 'people',
          name: 'Usuarios',
          visible:
            user.rol !== null
              ? user.rol.description === 'Administrador'
              : false,
          message: 'Administre los usuarios',
          redirect: 'users-admin',
        },
        {
          icon: 'paid',
          name: 'Comisiones',
          visible:
            user.rol !== null
              ? user.rol.description === 'Administrador' ||
                user.rol.description === 'Comercial'
              : false,
          message: 'Este al tanto de sus comisiones',
          redirect: `commissions/${user.id}`,
        },
      ];
    }
  }

  ngOnInit(): void {
    // <!-- <p><button mat-button (click)="sidenav.toggle()">Toggle</button></p> -->
    this.store.select('sidenav').subscribe(({ sidenav }) => {
      if (this.sidenav) {
        this.sidenav.toggle();
      }
    });
  }

  redirect(route: string) {
    this.router.navigateByUrl(route);
  }
}
