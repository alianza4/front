import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { NavbarComponent } from './components/home/navbar/navbar.component';
import { FooterComponent } from './components/home/footer/footer.component';
import { AuthComponent } from './components/auth/auth.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Components
import { HoverCardDirective } from './directives/hover-card.directive';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UserComponent } from './components/user/user.component';
import { DialogDeleteComponent } from './components/dialog-delete/dialog-delete.component';
import { PropertiesComponent } from './components/properties/properties.component';
import { CardComponent } from './components/card/card.component';
import { UserPropertiesComponent } from './components/user-properties/user-properties.component';

// Angular Material
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

// NGRX
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { appReducers } from './store/app.reducers';
import { InterceptorService } from './services/interceptor.service';
import { AddPropComponent } from './components/user-properties/add-prop/add-prop.component';
import { FilterPropsPipe } from './pipes/filter-props.pipe';
import { UsersAdminComponent } from './components/users-admin/users-admin.component';
import { AddUserComponent } from './components/users-admin/add-user/add-user.component';
import { PropertyDetailComponent } from './components/property-detail/property-detail.component';
import { CarouselImgComponent } from './components/carousel-img/carousel-img.component';
import { ListCommissionsComponent } from './components/list-commissions/list-commissions.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    FooterComponent,
    AuthComponent,
    UserComponent,
    DialogDeleteComponent,
    PropertiesComponent,
    CardComponent,
    HoverCardDirective,
    SidebarComponent,
    UserPropertiesComponent,
    AddPropComponent,
    FilterPropsPipe,
    UsersAdminComponent,
    AddUserComponent,
    PropertyDetailComponent,
    CarouselImgComponent,
    ListCommissionsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatSliderModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatMenuModule,
    MatDialogModule,
    MatCardModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    StoreModule.forRoot(appReducers),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
