export class RequestResult<T> {
  successful: boolean;
  error: boolean;
  errorMessage: string;
  messages: string[];
  result: T;

  constructor(result: T) {
    this.successful = false;
    this.error = false;
    this.errorMessage = '';
    this.messages = [];
    this.result = result;
  }
}
