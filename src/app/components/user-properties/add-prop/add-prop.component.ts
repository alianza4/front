import { Description } from './../../../models/description';
import { OfferType } from './../../../models/enum';
import { PropertyService } from './../../../services/property.service';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { SESSION } from 'src/app/models/const';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RequestResult } from 'src/app/models/requestResult';

@Component({
  selector: 'app-add-prop',
  templateUrl: './add-prop.component.html',
  styleUrls: ['./add-prop.component.scss'],
})
export class AddPropComponent implements OnInit {
  registerForm = this.fb.group({
    id: [''],
    offerType: [OfferType.SALE],
    idTypeProperty: [''],
    idCity: [''],
    idNeighbourhood: [''],
    address: [''],
    price: [0],
    stratum: [0],
    area: [0],
    administrationInPrice: [false],
    rooms: [0],
    bathrooms: [0],
    floors: [0],
    description: [''],
    idImage: [null],
    parking: [false],
    idUser: [''],
    state: ['OFFER'],
    userProperties: this.fb.array([]),
  });

  typeProperties: Description[] = [];
  cities: Description[] = [];
  neighbourhoods: Description[] = [];

  offersType: any[] = [
    { name: 'Venta', value: OfferType.SALE },
    { name: 'Arriendo', value: OfferType.RENTAL },
  ];

  stratum: number[] = Array.from({ length: 6 }, (_, i) => i + 1);

  booleans: any[] = [
    { name: 'Sí', value: true },
    { name: 'No', value: false },
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AddPropComponent>,
    private fb: FormBuilder,
    private propertyService: PropertyService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    let session = JSON.parse(atob(localStorage.getItem(SESSION)!));
    this.registerForm.patchValue({ idUser: session.id });

    this.getTypeProperties();
    this.getCities();

    if (this.data != null) this.setValueToForm();
  }

  get usersProps(): FormArray {
    return this.registerForm.get('userProperties') as FormArray;
  }

  setValueToForm() {
    this.registerForm.patchValue(this.data.name);
    this.getByCity(this.registerForm.value.idCity);
  }

  getTypeProperties() {
    this.propertyService.getTypeProperties().subscribe((rq) => {
      if (rq.successful) {
        this.typeProperties = rq.result;
      }
    });
  }

  getCities() {
    this.propertyService.getCities().subscribe((rq) => {
      if (rq.successful) {
        this.cities = rq.result;
      }
    });
  }

  getByCity(idCity: string) {
    this.propertyService.getByCity(idCity).subscribe((rq) => {
      if (rq.successful) {
        this.neighbourhoods = rq.result;
      }
    });
  }

  onChangeCity(e: any) {
    this.getByCity(e.value);
  }

  register() {
    let session = JSON.parse(atob(localStorage.getItem(SESSION)!));

    this.registerForm.patchValue({
      price: parseInt(this.registerForm.value.price),
    });
    this.registerForm.patchValue({
      area: parseInt(this.registerForm.value.area),
    });

    this.usersProps.push(this.fb.group({ idUser: [session.id] }));

    this.propertyService.save(this.registerForm.value).subscribe((rq) => {
      if (rq.successful) {
        this.snackBar.open('Creado correctamente', 'Cerrar', {
          duration: 3000,
          panelClass: ['snackbar-success'],
        });

        this.dialogRef.close();
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
