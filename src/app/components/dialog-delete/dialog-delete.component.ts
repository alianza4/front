import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-delete',
  templateUrl: './dialog-delete.component.html',
  styleUrls: ['./dialog-delete.component.scss'],
})
export class DialogDeleteComponent {
  constructor(
    public dialog: MatDialogRef<DialogDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public mensaje: string
  ) {}

  cerrarDialogo(): void {
    this.dialog.close(false);
  }
  confirmado(): void {
    this.dialog.close(true);
  }
}
