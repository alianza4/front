import { RequestResult } from '../../../models/requestResult';
import { AuthService } from '../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserToken } from 'src/app/models/userToken';
import { SESSION, TOKEN } from 'src/app/models/const';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  hide = true;

  loginForm = this.fb.group({
    email: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {}

  login() {
    if (this.loginForm.invalid) return;

    this.authService.login(this.loginForm.value).subscribe((rq) => {
      if (rq.successful) {
        this.createSession(rq.result);
        this.router.navigateByUrl('home');
        setTimeout(() => {
          location.reload();
        }, 1000);
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  createSession(user: UserToken) {
    let userString = btoa(JSON.stringify(user));
    localStorage.setItem(SESSION, userString);
    localStorage.setItem(TOKEN, btoa(user.token));
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
