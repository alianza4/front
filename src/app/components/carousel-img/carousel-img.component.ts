import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel-img',
  templateUrl: './carousel-img.component.html',
  styleUrls: ['./carousel-img.component.scss'],
})
export class CarouselImgComponent implements OnInit {
  slideIndex = 1;
  constructor() {}

  ngOnInit(): void {
    this.showDivs(this.slideIndex);
  }

  currentDiv(n: any) {
    this.showDivs((this.slideIndex = n));
  }

  plusDivs(n: any) {
    this.showDivs((this.slideIndex += n));
  }

  showDivs(n: any) {
    var i;
    var x = document.getElementsByClassName(
      'mySlides'
    ) as HTMLCollectionOf<HTMLElement>;
    var dots = document.getElementsByClassName('demo');
    if (n > x.length) {
      this.slideIndex = 1;
    }
    if (n < 1) {
      this.slideIndex = x.length;
    }
    for (i = 0; i < x.length; i++) {
      x[i].style.display = 'none';
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(' w3-white', '');
    }
    x[this.slideIndex - 1].style.display = 'block';
    dots[this.slideIndex - 1].className += ' w3-white';
  }
}
