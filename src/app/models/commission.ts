export class Commission {
  public id!: string;
  public amount!: number;
  public comment!: string;
  public buyer!: string;
  public idProperty!: string;
  public idUser!: string;
}
