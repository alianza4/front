import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Property } from 'src/app/models/property';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() property: Property = new Property();

  constructor(private router: Router) {}

  ngOnInit(): void {}

  goToProp() {
    this.router.navigate(['property', this.property.id]);
  }
}
