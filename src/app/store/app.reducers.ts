import { ActionReducerMap } from '@ngrx/store';
import * as reducers from './reducers';

export interface AppState {
  sidenav: reducers.State;
}

export const appReducers: ActionReducerMap<AppState> = {
  sidenav: reducers.reducer,
};
