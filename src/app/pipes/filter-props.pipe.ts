import { Pipe, PipeTransform } from '@angular/core';
import { Property } from '../models/property';

@Pipe({
  name: 'filterProps',
})
export class FilterPropsPipe implements PipeTransform {
  transform(props: Property[], filter: string): Property[] {
    var propsFilter = props.filter((prop) => {
      return (
        prop.city.description.toLowerCase().indexOf(filter.toLowerCase()) >
          -1 ||
        prop.neighbourhood.description
          .toLowerCase()
          .indexOf(filter.toLowerCase()) > -1
      );
    });

    return propsFilter;
  }
}
