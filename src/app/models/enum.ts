export enum OfferType {
  SALE = "SALE",
  RENTAL = "RENTAL",
}

export enum StateProperty {
  SALE = "SALE",
  OFFER = "OFFER",
}
