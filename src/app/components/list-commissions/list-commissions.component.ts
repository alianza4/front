import { ActivatedRoute } from '@angular/router';
import { CommissionService } from './../../services/commission.service';
import { Commission } from './../../models/commission';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { RequestResult } from 'src/app/models/requestResult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-list-commissions',
  templateUrl: './list-commissions.component.html',
  styleUrls: ['./list-commissions.component.scss'],
})
export class ListCommissionsComponent implements OnInit {
  id!: string;
  commissions: Commission[] = [];

  dataSource!: MatTableDataSource<Commission>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  displayedColumns: string[] = [
    'amount',
    'buyer',
    'idProperty',
    'idUser',
    'comment',
  ];

  constructor(
    private commissionService: CommissionService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.id = this.route.snapshot.paramMap.get('id')!;
  }

  ngOnInit(): void {
    this.getCommissions();
  }

  export() {
    this.commissionService.exportReport(this.id).subscribe((result) => {
      var blob = new Blob([result]);
      FileSaver.saveAs(blob, 'comisiones.pdf');
    });
  }

  getCommissions() {
    this.commissionService.getCommissions(this.id).subscribe((rq) => {
      if (rq.successful) {
        this.commissions = rq.result;
        this.dataSource = new MatTableDataSource(this.commissions);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
