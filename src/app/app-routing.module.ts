import { ListCommissionsComponent } from './components/list-commissions/list-commissions.component';
import { PropertyDetailComponent } from './components/property-detail/property-detail.component';
import { UsersAdminComponent } from './components/users-admin/users-admin.component';
import { UserPropertiesComponent } from './components/user-properties/user-properties.component';
import { PropertiesComponent } from './components/properties/properties.component';
import { UserComponent } from './components/user/user.component';
import { AuthComponent } from './components/auth/auth.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'user/:id', component: UserComponent },
  { path: 'properties', component: PropertiesComponent },
  { path: 'user-properties/:id', component: UserPropertiesComponent },
  { path: 'users-admin', component: UsersAdminComponent },
  { path: 'property/:id', component: PropertyDetailComponent },
  { path: 'commissions/:id', component: ListCommissionsComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
