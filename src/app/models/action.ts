export class Action {
  name!: string;
  icon!: string;
  visible!: boolean;
  message!: string;
  redirect!: string;
}
