import { Action, createReducer, on } from '@ngrx/store';
import * as actions from '../actions';

export interface State {
  sidenav: boolean;
}

export const initialState: State = {
  sidenav: false,
};

const sidenavReducer = createReducer(
  initialState,
  on(actions.toggle, (state) => ({ ...state, sidenav: !state.sidenav }))
);

export function reducer(state: State | undefined, action: Action) {
  return sidenavReducer(state, action);
}
