import { TOKEN } from '../../../models/const';
import { UserToken } from '../../../models/userToken';
import { AuthService } from '../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RequestResult } from 'src/app/models/requestResult';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SESSION } from 'src/app/models/const';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  hide = true;

  registerForm = this.fb.group({
    name: ['', Validators.required],
    lastName: ['', Validators.required],
    phone: [''],
    email: [''],
    password: ['', Validators.required],
    idRol: ['3c6639d8-5bb4-4df8-b65f-24a221a04058'],
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {}

  register() {
    if (this.registerForm.invalid) return;

    this.authService.register(this.registerForm.value).subscribe((rq) => {
      if (rq.successful) {
        this.createSession(rq.result);
        this.router.navigateByUrl('home');
        setTimeout(() => {
          location.reload();
        }, 1000);
      } else {
        this.validateRequestResult(rq);
      }
    });
  }

  createSession(user: UserToken) {
    let userString = btoa(JSON.stringify(user));
    localStorage.setItem(SESSION, userString);
    localStorage.setItem(TOKEN, btoa(user.token));
  }

  validateRequestResult(rq: RequestResult<any>) {
    if (rq.error) {
      this.snackBar.open(rq.errorMessage, 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    } else {
      this.snackBar.open(rq.messages.join(','), 'Cerrar', {
        duration: 3000,
        panelClass: ['snackbar-error'],
      });
    }
  }
}
