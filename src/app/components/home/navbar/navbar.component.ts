import { SESSION } from '../../../models/const';
import { UserToken } from '../../../models/userToken';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducers';
import * as sidenavActions from '../../../store/actions';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Input() visible = false;
  isSession = false;
  user: UserToken = new UserToken();

  constructor(private router: Router, private store: Store<AppState>) {
    var userSession = localStorage.getItem(SESSION);
    if (userSession != null) {
      this.isSession = true;
      this.user = JSON.parse(atob(userSession));
    }
  }

  ngOnInit(): void {}

  toggle() {
    this.store.dispatch(sidenavActions.toggle());
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/home');
    setTimeout(() => {
      location.reload();
    }, 1000);
  }
}
